<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <div class="container mt-4">
        <h2> Détails du stagiaire numéro {{ $stagiaire->id }}</h2>
        <div class="form-group">
            @if(isset($stagiaire->photo ))
                    <img src="{{asset('photos/'.$stagiaire->photo) }}" alt="photo" width="400px"> 
            @else
                   <img src="{{asset('photos/default.jpg') }}" alt="photo" width="200px"> 
            @endif
        </div> 
        <div class="form-group">
            <strong>Nom complet :</strong>
            {{ $stagiaire->nom_complet }}
        </div>
        <div class="form-group">
           <strong>Genre :</strong>
           {{ $stagiaire->genre }}
        </div>
        <div class="form-group">
            <strong>Date de naissance :</strong>
            {{ $stagiaire->date_naissance->format("d/m/Y") }}
        </div>
        <div class="form-group">
            <strong>Note :</strong>
            {{ $stagiaire->note }}
        </div>
        <div class="form-group">
            <strong>Groupe :</strong>
            {{ $stagiaire->groupe_id }}
        </div>
        <div class="form-group">
            <strong>Création :</strong>
            {{ $stagiaire->created_at->format("d/m/Y H:i") }}
        </div>
        <div class="form-group">
            <strong>Dernière modification :</strong>
            {{ $stagiaire->updated_at->format("d/m/Y H:i") }}
        </div>
       </div>
 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

</body>
</html>