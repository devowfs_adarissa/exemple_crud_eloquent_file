<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Document</title>
</head>
 
<body class="container mt-2">
      @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>    
    </div>
    @endif
<h2>Nouveau stagiaire</h2>
   <form class="col-8" method="post" action="{{route('stagiaire.store')}}" enctype="multipart/form-data">
       @csrf
  <div class="mb-3">
    <label for="nom_complet" class="form-label">Nom complet</label>
    <input type="text" class="form-control" id="nom_complet" name="nom_complet" value="{{ old('nom_complet') }}"/>
  </div>
    <div class="mb-3">
        <label for="genre" class="form-label">Genre : </label>
        <div class="form-check form-check-inline">
            <input class="form-check-input " type="radio" name="genre" id="genre_f" value="F">
            <label class="form-check-label" for="genre_f"> F </label>
        </div>
         <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="genre" id="genre_m" value="M">
            <label class="form-check-label" for="genre_m"> M </label>
        </div>
    </div>
    <div class="mb-3">
        <label for="date_naissance" class="form-label">Date de naissance</label>
        <input type="date" class="form-control" id="date_naissance" name="date_naissance"/>
    </div>
    <div class="mb-3">
        <label for="note" class="form-label">Note</label>
        <input type="text" class="form-control" id="note" name="note"/>
    </div>
    <div class="mb-3">
        <label for="groupe_id" class="form-label">Groupe</label>
        <input type="text" class="form-control" id="groupe_id" name="groupe_id"/>
    </div>
    <div class="mb-3">
        <label for="photo" class="form-label">Photo</label>
        <input type="file" class="form-control" id="photo" name="photo"/>
    </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>
</html>