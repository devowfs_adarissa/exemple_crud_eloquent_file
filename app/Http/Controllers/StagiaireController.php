<?php

namespace App\Http\Controllers;

use App\Models\Stagiaire;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class StagiaireController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $stagiaires=Stagiaire::all();
        return view("stagiaire.index", compact("stagiaires"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("stagiaire.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom_complet' => 'required|regex:/^[A-Za-z\s]+$/',
            'date_naissance' => 'required|date',
            'note' => 'required|numeric|between:0,20',
            'genre'=>'in:M,F',
             'photo' => 'image|mimes:png,jpg,jpeg|max:8000'
        ]);

        $photoName=null;
        if(isset($request->photo)){
             $photoName = time().'.'.$request->photo->extension();

            //Stockage dans le dossier "Stockage"
            $request->photo->storeAs('photos', $photoName);
        }
       

        Stagiaire::create([
            "nom_complet"=>$request->nom_complet,
            "genre"=>$request->genre,
            "date_naissance"=>$request->date_naissance,
            "note"=>$request->note,
            "groupe_id"=>$request->groupe_id,
            "photo"=>$photoName
        ]);

        return redirect()->route('stagiaire.index')->with('success','Le stagiaire a été ajouté avec succès.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Stagiaire $stagiaire)
    {
        return view("stagiaire.show", compact("stagiaire"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Stagiaire $stagiaire)
    {
        return view("stagiaire.edit", compact("stagiaire"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Stagiaire $stagiaire)
    {
         $request->validate([
            'nom_complet' => 'required|regex:/^[A-Za-z\s]+$/',
            'date_naissance' => 'required|date',
            'note' => 'required|numeric|between:0,20',
            'genre'=>'in:M,F',
             'photo' => 'image|mimes:png,jpg,jpeg|max:8000'
        ]);

        $photoName = time().'.'.$request->photo->extension();

        //Stockage dans le dossier "Stockage"
        $request->photo->storeAs('photos', $photoName);

        //Supprimer l'ancienne photo
       if(isset($stagiaire->photo))
            Storage::delete("photos/". $stagiaire->photo);

        $stagiaire->nom_complet=$request->nom_complet;
        $stagiaire->genre=$request->genre;
        $stagiaire->date_naissance=$request->date_naissance;
         $stagiaire->note=$request->note;
        $stagiaire->groupe_id=$request->groupe_id;
        $stagiaire->photo=$photoName;
        $stagiaire->save();


        return redirect()->route('stagiaire.index')->with('success','Le stagiaire a été modifié avec succès.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Stagiaire $stagiaire)
    {
       //Supprimer la photo
       if(isset($stagiaire->photo))
            Storage::delete("photos/". $stagiaire->photo);

        $stagiaire->delete();
        return redirect()->route('stagiaire.index')->with('success','Le stagiaire a été supprimé avec succès.');

    }
}
